<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function index(){
        $data=DB::table('cast')->get();
        return view('cast.index',compact('data'));
    }

    public function create(){
        return view('cast.create');
    }

    public function store(Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required|integer',
            'bio'  => 'required'
        ]);
        $query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect('/cast');
    }

    public function show($id){
        $data=DB::table('cast')->where('id',$id)->first();
        return view('cast.show',compact('data'));
    }

    public function edit($id){
        $data=DB::table('cast')->where('id',$id)->first();
        return view('cast.edit',compact('data'));
    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required|integer',
            'bio'  => 'required'
        ]);

        $query = DB::table('cast')
            ->where('id', $id)
            ->update([
                "nama" => $request["nama"],
                "umur" => $request["umur"],
                "bio" => $request["bio"]
            ]);
        return redirect('/cast');
    }

    public function destroy($id){
        $query=DB::table('cast')->where('id',$id)->delete();
        return redirect('/cast');
    }
}
