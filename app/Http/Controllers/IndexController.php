<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    //Latihan Laravel Web Statis
    // public function index(){
    //     return view('index');
    // }


    //Latihan Template
    public function index(){
        return view('page.dashboard');
    }

    public function table(){
        return view('page.table');
    }

    public function datatable(){
        return view('page.datatable');
    }
}
