@extends('layout.master')

@section('title')
    Detail Cast
@endsection

@section('subtitle')
   Data Cast ID {{$data->id}}
@endsection

@section('content')
<h5>Nama :</h5>
<h5><b>{{$data->nama}}</b></h5><br>
<h5>Umur :</h5>
<h5><b>{{$data->umur}}</b> Tahun</h5><br>
<h5>Bio :</h5>
<p><b>{{$data->bio}}</b></p>
@endsection