@extends('layout.master')

@section('title')
    Edit Cast
@endsection

@section('subtitle')
   Form Edit Cast ID {{$data->id}}
@endsection

@section('content')
<div>
    <form action="/cast/{{$data->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="nama">Nama:</label>
            <input type="text" class="form-control" name="nama" value="{{$data->nama}}" id="nama" placeholder="Masukkan Nama">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="umur">Umur:</label>
            <input type="text" class="form-control" name="umur"  value="{{$data->umur}}"  id="umur" placeholder="Masukkan Umur">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Bio:</label>
            <textarea class="form-control" name="bio" id="bio" placeholder="Masukkan Bio">{{$data->bio}}</textarea>
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>
@endsection