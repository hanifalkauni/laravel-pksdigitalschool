@extends('layout.master')

@section('title')
    Cast
@endsection

@section('subtitle')
   List Cast
@endsection

@section('content')
<a href="/cast/create" class="btn btn-success"><i class="fa fa-plus-square"></i> Create Data</a>
        <table class="table table-bordered table-striped" id="cast">
            <thead class="thead-dark">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Age</th>
                <th scope="col" >Actions</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($data as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>{{$value->umur}}</td>
                        <td>
                            <a href="/cast/{{$value->id}}" class="btn btn-info"><i class="fa fa-info-circle"></i> Detail</a>
                            <a href="/cast/{{$value->id}}/edit" class="btn btn-warning"><i class="fa fa-edit"></i> Edit</a>
                            <form action="/cast/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                               <input type="submit" class="btn btn-danger my-1 " value="Delete">
                            </form>
                        </td>
                    </tr>
                {{-- @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse--}}
                    @endforeach
            </tbody>
        </table>
@endsection

@push('script')
<script src="{{asset('AdminLTE/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('AdminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#cast").DataTable();
  });
</script>
@endpush